import java.io.File;
public class rmdir {
    public static void main(String[] args){
        String filename = args[0];
        File file = new File(filename);
        deleteFolder(file);
    }

    static void deleteFolder(File filename){
        for(File subFile : filename.listFiles()){
            if(subFile.isDirectory()){
                deleteFolder(subFile);
            }else{
                subFile.delete();
            }
        }
        filename.delete();
    }
}
