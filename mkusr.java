import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
class mkusr{
    public static void main(String[] args) throws IOException{
        String username = args[0];
        String userbin;

        username = "../usr/" + username;
        userbin = username + "/.bin";
        
        File userdir = new File(username);
        if(!userdir.exists()){
            userdir.mkdirs();
            File bindir = new File(userbin);
            bindir.mkdir();
            binContents(userbin);
        }else{
            System.err.println("username already exists.");
        }
    }
    static void binContents(String usrbin) throws IOException{
        String binDir = "../bin";

        File bin = new File(binDir);
        File binContent[] = bin.listFiles();

        int binLen = binContent.length;
        int index = 0;

        FileInputStream inStream;
        FileOutputStream outStream;
        byte[] buffer = new byte[1024];
        int length;

        while(index <= binLen){
            inStream = new FileInputStream("../bin/" + binContent[index].getName());
            outStream = new FileOutputStream(usrbin + "/" + binContent[index].getName());
            while((length = inStream.read(buffer)) > 0){
                outStream.write(buffer, 0, length);
            }
            index = index + 1;
            if(index == binLen){
                break;
            }
        }
    }
}