import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
class cp{
    public static void main(String[] args) throws IOException{
        String source = args[0];
        String destination = args[1];

        FileInputStream inStream;
        FileOutputStream outStream;

        try{
            inStream = new FileInputStream(source);
            outStream = new FileOutputStream(destination);
            byte[] buffer = new byte[1024];
            int length;
            while((length = inStream.read(buffer)) > 0){
                outStream.write(buffer, 0, length);
            }
        } catch(IOException e){
            e.printStackTrace();
        }
    }
}