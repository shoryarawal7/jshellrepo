import java.io.File;
import java.io.IOException;
class mkdir{
	public static void main(String[] args) throws IOException{
		String filename = args[0];
			File newDir = new File(filename);
			if(!newDir.exists()){
				newDir.mkdirs();
			}else{
				System.out.println(filename + " already exists.");
			}
	}
}
