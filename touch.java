import java.io.File;
import java.io.IOException;

public class touch{
    public static void main(String[] args)throws IOException{
        String filename = args[0];
        try{
            File file = new File(filename);
            if(file.createNewFile()){
                System.out.println("File created.");
            }else{
                System.out.println("File already exists.");
            }
        } catch(IOException e){
            System.err.println("File already exists");
        }
    }
}